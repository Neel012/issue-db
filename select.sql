-- select comments for given issue sorted 
SELECT U.nickname, C.date, C.content
FROM (Comments C INNER JOIN Users U ON C.userID=U.userID)
    INNER JOIN Issues I ON C.issueID=I.issueID
WHERE I.title='Issue #1'
ORDER BY C.date ASC;

-- select comment thread with identation
WITH RECURSIVE comment_thread(commentID, parentCommentID, tissueID, tuserID,
                              tdate, tcontent, lvl) AS (
    SELECT C.commentID, C.parentCommentID, C.issueID, C.userID, C.date, C.content, 0
    FROM Comments C
    WHERE parentCommentID IS NULL
UNION ALL
    SELECT C.commentID, C.parentCommentID, C.issueID, C.userID, C.date, C.content,
           comment_thread.lvl+1
    FROM Comments C
    JOIN comment_thread
    ON C.parentCommentID=comment_thread.commentID
    ORDER BY 7 DESC, C.date ASC
)
SELECT SUBSTR('..............', 1, lvl*3) || commentID, issueID, userID, date, content
FROM comment_thread C
INNER JOIN Issues I ON C.tissueID=I.issueID
WHERE I.title='Issue #1';

-- number of comments from one user
SELECT nickname, COUNT(*)
FROM Users U
INNER JOIN Comments C
ON U.userID=C.userID
GROUP BY U.userID;

SELECT nickname, COUNT(*)
FROM Users U
INNER JOIN Issues I
ON U.userID=I.userID
GROUP BY U.userID;


-- children of an issue
SELECT issueID, title
FROM Issues
WHERE parentIssueID=(
    SELECT issueID
    FROM Issues
    WHERE title='Issue #3');

SELECT AVG(LENGTH(content)) FROM Comments;

SELECT SUM(LENGTH(content))
FROM (
    SELECT content FROM Comments
    UNION ALL
    SELECT content FROM Issues
);
