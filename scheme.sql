CREATE TABLE Issues (
    issueID INTEGER PRIMARY KEY,
    parentIssueID INTEGER REFERENCES Issues(issueID),
    triageID INTEGER NOT NULL REFERENCES Triages(triageID),
    userID INTEGER NOT NULL REFERENCES Users(userID),
    date datetime NOT NULL,
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL
);

CREATE TABLE Comments (
    commentID INTEGER PRIMARY KEY,
    parentCommentID INTEGER REFERENCES Comments(commentID),
    issueID INTEGER NOT NULL REFERENCES Issue(issueID),
    userID INTEGER NOT NULL REFERENCES Users(userID),
    date DATETIME NOT NULL,
    content TEXT NOT NULL
);

CREATE TABLE IssueBlockers (
    blockeeID INTEGER NOT NULL REFERENCES Issue(issueID),
    blockerID INTEGER NOT NULL REFERENCES Issues(issueID), 
    CHECK (blockeeID != blockerID),
    PRIMARY KEY (blockeeID, blockerID)
);

CREATE TABLE Triages (
    triageID INTEGER PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE Users (
    userID INTEGER PRIMARY KEY,
    nickname VARCHAR(255)
);

CREATE TABLE Roles (
    roleID INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE UserRoles (
    userID INTEGER NOT NULL REFERENCES Users(userID),
    roleID INTEGER NOT NULL REFERENCES Roles(roleID),
    PRIMARY KEY (userID, roleID)
);

CREATE TABLE IssueRoles (
    issueID INTEGER NOT NULL REFERENCES Issues(issueID),
    roleID INTEGER NOT NULL REFERENCES Roles(roleID),
    PRIMARY KEY (issueID, roleID)
);

CREATE TABLE Tags (
    tagID INTEGER PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

CREATE TABLE IssueTags (
    issueID INTEGER NOT NULL REFERENCES Issues(issueID),
    tagID INTEGER NOT NULL REFERENCES Tags(tagID),
    PRIMARY KEY(issueID, tagID)
);

CREATE TABLE IssueTriageLog (
    issueID INTEGER NOT NULL REFERENCES Issues(issueID),
    triageID INTEGER NOT NULL REFERENCES Triages(name),
    date DATETIME NOT NULL,
    PRIMARY KEY (issueID, date)
);
