---
author: Ondřej Matušák
title: Tiketovací systém
---

Popis a vytvoření tabulek
---

Databáze obsahuje tabulky s názvy Issues, Comments, IssueBlockers, Triages, Users, Roles, UserRoles, IssueRoles, Tags, IssueTags a IssueTriageLog. 

Tabulka Issues obsahuje všechny tikety v systému. Každý tiket může mít libovolný počet sub-tiketů pro sledování dílčích úkolů.
```sql
CREATE TABLE Issues (
    issueID INTEGER PRIMARY KEY,
    parentIssueID INTEGER REFERENCES Issues(issueID),
    triageID INTEGER NOT NULL REFERENCES Triages(triageID),
    userID INTEGER NOT NULL REFERENCES Users(userID),
    date datetime NOT NULL,
    title VARCHAR(255) NOT NULL,
    content TEXT NOT NULL
);
```

Tabulka Comments obshuje komentáře k jedotlivým tiketům. Každý komentář může mít podkomentáře.
```sql
CREATE TABLE Comments (
    commentID INTEGER PRIMARY KEY,
    parentCommentID INTEGER REFERENCES Comments(commentID),
    issueID INTEGER NOT NULL REFERENCES Issue(issueID),
    userID INTEGER NOT NULL REFERENCES Users(userID),
    date DATETIME NOT NULL,
    content TEXT NOT NULL
);
```

Tabulka IssueBlockers představuje vztah mezi tikety blokujícími a blokovanými.
```sql
CREATE TABLE IssueBlockers (
    blockeeID INTEGER NOT NULL REFERENCES Issue(issueID),
    blockerID INTEGER NOT NULL REFERENCES Issues(issueID), 
    CHECK (blockeeID != blockerID),
    PRIMARY KEY (blockeeID, blockerID)
);
```

Tabulka Triages představuje aktuálnost tiketu ('Now', 'Later', 'Done').
```sql
CREATE TABLE Triages (
    triageID INTEGER PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);

INSERT INTO Triages(name) VALUES
    ('Now'),
    ('Later'),
    ('Done');
```

Tabulka Users obsahuje uživatele systému.
```sql
CREATE TABLE Users (
    userID INTEGER PRIMARY KEY,
    nickname VARCHAR(255)
);
```

Tabulky Roles a UserRolessledují privilegia jednotlivých uživatelů.
```sql
CREATE TABLE Roles (
    roleID INTEGER PRIMARY KEY,
    name TEXT NOT NULL
);

CREATE TABLE UserRoles (
    userID INTEGER NOT NULL REFERENCES Users(userID),
    roleID INTEGER NOT NULL REFERENCES Roles(roleID),
    PRIMARY KEY (userID, roleID)
);
```

Tabulka IssueRoles sleduje potřebná privilegia pro přístup k jednotlivým tiketům.
```sql
IssueRoles CREATE TABLE IssueRoles (
    issueID INTEGER NOT NULL REFERENCES Issues(issueID),
    roleID INTEGER NOT NULL REFERENCES Roles(roleID),
    PRIMARY KEY (issueID, roleID)
);
```

Tabulka Tags obsahuje všechny možné štítky které moho být připnuty k tiketu.
```sql
CREATE TABLE Tags (
    tagID INTEGER PRIMARY KEY,
    name VARCHAR(255) NOT NULL
);
```

Tabulka IssueTags představuje vztah mezi tickety a tagy.
```sql
CREATE TABLE IssueTags (
    issueID INTEGER NOT NULL REFERENCES Issues(issueID),
    tagID INTEGER NOT NULL REFERENCES Tags(tagID),
    PRIMARY KEY(issueID, tagID)
);
```

Tabulka IssueTriageLog obsahuje záznamy změn aktuálnosti tiketů.
```sql
CREATE TABLE IssueTriageLog (
    issueID INTEGER NOT NULL REFERENCES Issues(issueID),
    triageID INTEGER NOT NULL REFERENCES Triages(name),
    date DATETIME NOT NULL,
    PRIMARY KEY (issueID, date)
);
```

Naplnění daty
---
```sql
insert into Users(nickname) values
    ('Cathern Nakken'),
    ('Margarito Kyllonen'),
    ('Shakita Klemm'),
    ('Nina Levalley'),
    ('Sherilyn Champlin'),
    ('Karin Boaz'),
    ('Shameka Ballantine'),
    ('Niesha Busey'),
    ('Lachelle Schuck'),
    ('Vern Pung'),
    ('Kourtney Beckmann'),
    ('Madison Dungan'),
    ('Lexie Denardo'),
    ('Yuette Procopio'),
    ('Tommye Spring'),
    ('Waneta Hundt'),
    ('Anita Ellery'),
    ('Karissa Smock'),
    ('Tierra Blackburn'),
    ('Jennell Gillman');
```

Přidání tiketu
```sql
INSERT INTO Issues(triageID, userID, date, title, content) VALUES
(
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Tommye Spring'),
    datetime('now', 'utc'),
    'Issue #1',
    'Description...'
),
(
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Anita Ellery'),
    datetime('now', 'utc'),
    'Issue #2',
    'Description...'
),
(
    (SELECT name FROM Triages WHERE name='Later'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Issue #3',
    'Description...'
),
(
    (SELECT name FROM Triages WHERE name='Later'),
    (SELECT userID FROM Users WHERE nickname='Tommye Spring'),
    datetime('now', 'utc'),
    'Issue #4',
    'Description...'
);
```
Přidání podtiketů
```sql
INSERT INTO Issues(parentIssueID, triageID, userID, date, title, content) VALUES
(
    (SELECT issueID FROM Issues WHERE title='Issue #3'),
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Anita Ellery'),
    datetime('now', 'utc'),
    'Sub-issue #3-1',
    'Description...'
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #3'),
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Sub-issue #3-2',
    'Description...'
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #3'),
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Tommye Spring'),
    datetime('now', 'utc'),
    'Sub-issue #3-3',
    'Description...'
);
```
Přidání komentářů
```sql
INSERT INTO Comments(issueID, userID, date, content) VALUES
(
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Comment content...'
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Comment content...'
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Comment content...'
);
```
Přidání podkomentářů
```sql
INSERT INTO Comments(parentCommentID, issueID, userID, date, content) VALUES
(
    (SELECT commentID FROM Comments C INNER JOIN Issues I ON C.issueID=I.issueID
        WHERE title ='Issue #1' LIMIT 1
    ),
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    datetime('now', 'utc'),
    'Sub-Comment...'
),
(
    (SELECT commentID FROM Comments C INNER JOIN Issues I ON C.issueID=I.issueID
        WHERE title ='Issue #1' LIMIT 1
    ),
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    datetime('now', 'utc'),
    'Sub-Comment...'
),
(
    (SELECT commentID FROM Comments C INNER JOIN Issues I ON C.issueID=I.issueID
        WHERE title ='Issue #1' LIMIT 1
    ),
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    datetime('now', 'utc'),
    'Sub-Comment...'
),
(
    (SELECT commentID FROM Comments C INNER JOIN Issues I ON C.issueID=I.issueID
        WHERE title ='Issue #1' LIMIT 1
    ),
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    datetime('now', 'utc'),
    'Sub-Comment...'
);

```
Přidání závislostí mezi tikety.
```sql
INSERT INTO IssueBlockers(blockeeID, blockerID) VALUES
(
    (SELECT issueID FROM Issues WHERE title='Issue #3'),
    (SELECT issueID FROM Issues WHERE title='Issue #1')
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #4'),
    (SELECT issueID FROM Issues WHERE title='Issue #2')
);
```
Přidání štítků
```sql
INSERT INTO Tags(name) VALUES
    ('ui'),
    ('network'),
    ('security'),
    ('performance');
```
Přiřazení štítku k tiketu
```sql
INSERT INTO IssueTags(issueID, tagID)
    SELECT Issues.issueID, Tags.tagID 
    FROM Issues, Tags
    WHERE Issues.title='Issue #1' AND Tags.name='performance'; 
```
Vytvoření skupin pro správu přístupu.
```sql
INSERT INTO Roles(name) VALUES
    ('Public'), ('Internal'), ('Development'), ('Security');
```
Přiřazení uživatelů do skupin
```sql
INSERT INTO UserRoles(userID, roleID) VALUES
(
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    (SELECT roleID FROM Roles WHERE name='Development')
),
(
    (SELECT userID FROM Users WHERE nickname='Lachelle Schuck'),
    (SELECT roleID FROM Roles WHERE name='Public')
),
(
    (SELECT userID FROM Users WHERE nickname='Tommye Spring'),
    (SELECT roleID FROM Roles WHERE name='Security')
);
```
Přiřazení tiketu ke skupině
```sql
INSERT INTO IssueRoles(issueID, roleID) VALUES
(
    (SELECT issueID FROM Issues WHERE title='Issue #2'),
    (SELECT roleID FROM Roles WHERE name='Development')
);
```

Další Dotazy
---

Nastavení existujícího tiketu jako potomka jiného.
```sql
UPDATE Issues
SET parentIssueID=(SELECT issueID FROM Issues WHERE title='Issue #1')
WHERE title='Issue #2';
```
Splnění tiketu
```sql
UPDATE Issues
SET triageID=(SELECT triageID FROM Triages WHERE name='Done')
WHERE issueID in (SELECT issueID FROM Issues WHERE title='#Issue #1');
```

Zobrazit komentáře podle času přidání.
```sql
SELECT U.nickname, C.date, C.content
FROM (Comments C INNER JOIN Users U ON C.userID=U.userID)
    INNER JOIN Issues I ON C.issueID=I.issueID
WHERE I.title='Issue #1'
ORDER BY C.date ASC;
```
\pagebreak

Zobrazení komentářů ve vláknech seřazeně podle času přidání
```sql
WITH RECURSIVE comment_thread(commentID, parentCommentID, tissueID, tuserID,
                              tdate, tcontent, lvl) AS (
    SELECT C.commentID, C.parentCommentID, C.issueID, C.userID, C.date, C.content, 0
    FROM Comments C
    WHERE parentCommentID IS NULL
UNION ALL
    SELECT C.commentID, C.parentCommentID, C.issueID, C.userID, C.date, C.content,
           comment_thread.lvl+1
    FROM Comments C
    JOIN comment_thread
    ON C.parentCommentID=comment_thread.commentID
    ORDER BY 7 DESC, C.date ASC
)
SELECT SUBSTR('..............', 1, lvl*3) || commentID, issueID, userID, date, content
FROM comment_thread C
INNER JOIN Issues I ON C.tissueID=I.issueID
WHERE I.title='Issue #1';
```

Počet komentářů jednotlivých uživatelů
```sql
SELECT nickname, COUNT(*)
FROM Users U
INNER JOIN Comments C
ON U.userID=C.userID
GROUP BY U.userID;
```

Počet tiketů jednotlivých uživatelů
```sql
SELECT nickname, COUNT(*)
FROM Users U
INNER JOIN Issues I
ON U.userID=I.userID
GROUP BY U.userID;
```

Vlozit popis tiketu
```sql
update Comments set content='Description ...'
where issueID=(
    SELECT issueID FROM Issues WHERE title = '#Issue #3'
)
```

Smazaní štítku
```sql
DELETE FROM Tags
WHERE name='performance'; 
```

Zobrazit přímé potomky tiketu
```sql
SELECT issueID, title
FROM Issues
WHERE parentIssueID=(
    SELECT issueID
    FROM Issues
    WHERE title='Issue #3');
```

```sql
SELECT SUM(LENGTH(content))
FROM Issues;
```

Průměrná délka komentáře
```sql
SELECT AVG(LENGTH(content)) FROM Comments
```

Celková délka všech komentářů a popisů tiketů
```sql
SELECT SUM(LENGTH(content))
FROM (
    SELECT content FROM Comments
    UNION ALL
    SELECT content FROM Issues
);
```

![Vztahy relací](./databaseDiagram/diagrams/summary/relationships.implied.large.png)

![Issues](./databaseDiagram/diagrams/summary/Issues.1degree.png)

![Comments](./databaseDiagram/diagrams/summary/Comments.1degree.png)

![IssueBlockers](./databaseDiagram/diagrams/summary/IssueBlockers.1degree.png)

![Users](./databaseDiagram/diagrams/summary/Users.1degree.png)

![Triages](./databaseDiagram/diagrams/summary/Triages.1degree.png)

![IssueTriageLog](./databaseDiagram/diagrams/summary/IssueTriageLog.1degree.png)

![Tags](./databaseDiagram/diagrams/summary/Tags.1degree.png)

![IssueTags](./databaseDiagram/diagrams/summary/IssueTags.1degree.png)

![Roles](./databaseDiagram/diagrams/summary/Roles.1degree.png)

![UserRoles](./databaseDiagram/diagrams/summary/UserRoles.1degree.png)

![IssueRoles](./databaseDiagram/diagrams/summary/IssueRoles.1degree.png)


