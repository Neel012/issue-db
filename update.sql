UPDATE Issues
SET triageID=(SELECT triageID FROM Triages WHERE name='Done')
WHERE issueID in (SELECT issueID FROM Issues WHERE title='#Issue #1');
