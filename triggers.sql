CREATE TRIGGER log_triage_change AFTER INSERT ON Issues
BEGIN
    INSERT INTO IssueTriageLog(issueID, triageID, date) VALUES (
        new.issueID,
        new.triageID,
        datetime('now', 'utc')
    );
END;
