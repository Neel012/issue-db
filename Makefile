run:
	rm -f db.sqlite
	sqlite3 db.sqlite < scheme.sql
	sqlite3 db.sqlite < triggers.sql
	sqlite3 db.sqlite < insert.sql
	sqlite3 db.sqlite < select.sql
	sqlite3 db.sqlite < update.sql
	sqlite3 db.sqlite < delete.sql

pdf:
	schemaspy -t sqlite -dp /usr/share/java/sqlite.jar -db db.sqlite -u neel -o databaseDiagram -imageformat png
	pandoc -o Matusak-Ondrej.pdf Matusak-Ondrej.md
