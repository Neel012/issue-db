insert into Users(nickname) values
    ('Cathern Nakken'),
    ('Margarito Kyllonen'),
    ('Shakita Klemm'),
    ('Nina Levalley'),
    ('Sherilyn Champlin'),
    ('Karin Boaz'),
    ('Shameka Ballantine'),
    ('Niesha Busey'),
    ('Lachelle Schuck'),
    ('Vern Pung'),
    ('Kourtney Beckmann'),
    ('Madison Dungan'),
    ('Lexie Denardo'),
    ('Yuette Procopio'),
    ('Tommye Spring'),
    ('Waneta Hundt'),
    ('Anita Ellery'),
    ('Karissa Smock'),
    ('Tierra Blackburn'),
    ('Jennell Gillman');

insert into Triages(name) values
    ('Now'),
    ('Later'),
    ('Done');

-- creating issues
INSERT INTO Issues(triageID, userID, date, title, content) VALUES
(
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Tommye Spring'),
    datetime('now', 'utc'),
    'Issue #1',
    'Description...'
),
(
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Anita Ellery'),
    datetime('now', 'utc'),
    'Issue #2',
    'Description...'
),
(
    (SELECT name FROM Triages WHERE name='Later'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Issue #3',
    'Description...'
),
(
    (SELECT name FROM Triages WHERE name='Later'),
    (SELECT userID FROM Users WHERE nickname='Tommye Spring'),
    datetime('now', 'utc'),
    'Issue #4',
    'Description...'
);

-- creating sub-issues
INSERT INTO Issues(parentIssueID, triageID, userID, date, title, content) VALUES
(
    (SELECT issueID FROM Issues WHERE title='Issue #3'),
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Anita Ellery'),
    datetime('now', 'utc'),
    'Sub-issue #3-1',
    'Description...'
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #3'),
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Sub-issue #3-2',
    'Description...'
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #3'),
    (SELECT name FROM Triages WHERE name='Now'),
    (SELECT userID FROM Users WHERE nickname='Tommye Spring'),
    datetime('now', 'utc'),
    'Sub-issue #3-3',
    'Description...'
);

-- setting the sub-issue for an existing issue
UPDATE Issues
SET parentIssueID=(SELECT issueID FROM Issues WHERE title='Issue #1')
WHERE title='Issue #2';

INSERT INTO Comments(issueID, userID, date, content) VALUES
(
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Comment content...'
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Comment content...'
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Waneta Hundt'),
    datetime('now', 'utc'),
    'Comment content...'
);

INSERT INTO Comments(parentCommentID, issueID, userID, date, content) VALUES
(
    (SELECT commentID FROM Comments C INNER JOIN Issues I ON C.issueID=I.issueID
        WHERE title ='Issue #1' LIMIT 1
    ),
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    datetime('now', 'utc'),
    'Sub-Comment...'
),
(
    (SELECT commentID FROM Comments C INNER JOIN Issues I ON C.issueID=I.issueID
        WHERE title ='Issue #1' LIMIT 1
    ),
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    datetime('now', 'utc'),
    'Sub-Comment...'
),
(
    (SELECT commentID FROM Comments C INNER JOIN Issues I ON C.issueID=I.issueID
        WHERE title ='Issue #1' LIMIT 1
    ),
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    datetime('now', 'utc'),
    'Sub-Comment...'
),
(
    (SELECT commentID FROM Comments C INNER JOIN Issues I ON C.issueID=I.issueID
        WHERE title ='Issue #1' LIMIT 1
    ),
    (SELECT issueID FROM Issues WHERE title='Issue #1'),
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    datetime('now', 'utc'),
    'Sub-Comment...'
);

INSERT INTO IssueBlockers(blockeeID, blockerID) VALUES
(
    (SELECT issueID FROM Issues WHERE title='Issue #3'),
    (SELECT issueID FROM Issues WHERE title='Issue #1')
),
(
    (SELECT issueID FROM Issues WHERE title='Issue #4'),
    (SELECT issueID FROM Issues WHERE title='Issue #2')
);

INSERT INTO Tags(name) VALUES
    ('ui'),
    ('network'),
    ('security'),
    ('performance');

INSERT INTO IssueTags(issueID, tagID)
    SELECT Issues.issueID, Tags.tagID 
    FROM Issues, Tags
    WHERE Issues.title='Issue #1' AND Tags.name='performance'; 

INSERT INTO Roles(name) VALUES
    ('Public'), ('Internal'), ('Development'), ('Security');

INSERT INTO UserRoles(userID, roleID) VALUES
(
    (SELECT userID FROM Users WHERE nickname='Cathern Nakken'),
    (SELECT roleID FROM Roles WHERE name='Development')
),
(
    (SELECT userID FROM Users WHERE nickname='Lachelle Schuck'),
    (SELECT roleID FROM Roles WHERE name='Public')
),
(
    (SELECT userID FROM Users WHERE nickname='Tommye Spring'),
    (SELECT roleID FROM Roles WHERE name='Security')
);

INSERT INTO IssueRoles(issueID, roleID) VALUES
(
    (SELECT issueID FROM Issues WHERE title='Issue #2'),
    (SELECT roleID FROM Roles WHERE name='Development')
);
